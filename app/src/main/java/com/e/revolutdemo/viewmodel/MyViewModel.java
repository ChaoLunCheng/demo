package com.e.revolutdemo.viewmodel;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.e.revolutdemo.R;
import com.e.revolutdemo.models.DataModel;
import com.e.revolutdemo.repository.DataRepository;

import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class MyViewModel extends ViewModel {

    private MutableLiveData<Integer> _errorMessage = new MutableLiveData<>();
    private MutableLiveData<DataModel> _rateData;

    private final CompositeDisposable disposable = new CompositeDisposable();
    private DataRepository repository;


    @Inject
    public MyViewModel(DataRepository repository) {
        this.repository = repository;

    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null) {
            disposable.clear();

        }
    }

    public MutableLiveData<DataModel> getData() {
        if (_rateData == null) {
            _rateData = new MutableLiveData<>();
        }
        loadData();
        return _rateData;
    }

    public LiveData<Integer> getErrorMessage() {
        return _errorMessage;
    }

    private void loadData() {
        disposable.add(repository.dataModelSingle()
                .delay(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<DataModel>() {
                    @Override
                    public void onSuccess(DataModel dataModel) {
                        getData().setValue(dataModel);
                    }

                    @Override
                    public void onError(Throwable e) {
                        // the error for possibly no network connection
                        if (isSameCause(e, UnknownHostException.class)) {
                            _errorMessage.setValue(R.string.no_network);
                        } else {
                            // other error messages
                            _errorMessage.setValue(R.string.error);
                        }

                    }
                }));
    }

    private boolean isSameCause(Throwable e, Class<? extends Throwable> cl) {
        return cl.isInstance(e) || e.getCause() != null && isSameCause(e.getCause(), cl);
    }

    public void recyclerViewTouched(View view, InputMethodManager imm) {
        // if imm is not null and the keyboard is shown, dismiss the keyboard
        if (imm != null && imm.isAcceptingText()) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
